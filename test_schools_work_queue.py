# simple_task_queue_logging.py

import os, pickle, numpy as np, math, scipy, pystan
import logging
import multiprocessing
import os
import time
import pystan

#from tasks import get_word_counts

PROCESSES = int( multiprocessing.cpu_count() / 2 ) # div 2 to not depend on hyper threading

NUMBER_OF_TASKS = 10


def pickle_schools_model():

    schools_code = """
    data {
      int<lower=0> J;         // number of schools
      real y[J];              // estimated treatment effects
      real<lower=0> sigma[J]; // standard error of effect estimates
    }

    parameters {
      real mu;                // population treatment effect
      real<lower=0> tau;      // standard deviation in treatment effects
      vector[J] eta;          // unscaled deviation from mu by school
    }

    transformed parameters {
      vector[J] theta = mu + tau * eta;        // school treatment effects
    }

    model {
      target += normal_lpdf(eta | 0, 1);       // prior log-density
      target += normal_lpdf(y | theta, sigma); // log-likelihood
    }
    """

    model_schools = pystan.StanModel(model_code=schools_code)

    with open('./schools_model.pkl', 'wb') as target:
        pickle.dump(model_schools, target)


def fit_schools(iteration, name):

    import pystan

    with open('./schools_model.pkl', 'rb') as source:
        sm = pickle.load(source)

    schools_data = {"J": 8,
                    "y": [28, 8, -3, 7, -1, 1, 18, 12],
                    "sigma": [15, 10, 16, 11, 9, 11, 10, 18]}

    print('launching sampling', iteration)

    schools_fit_model = sm.sampling(data=schools_data, iter=1000, chains=4, n_jobs= 1)

    print('fitted!!!')

    #samples = schools_fit_model.extract(permuted=True)

    #with open('/home/ezamfir/ptmp/ezamfir/testing_pystan_with_multiprocessing/'+name+'samples_'+str(iteration)+'.pkl','wb') as target:
    #    pickle.dump(samples, target)


def create_logger():

    logger = multiprocessing.get_logger()
    logger.setLevel(logging.INFO)
    fh = logging.FileHandler("process.log")
    fmt = "%(process)d - %(threadName)s - %(asctime)s - %(levelname)s - %(message)s"
    formatter = logging.Formatter(fmt)
    fh.setFormatter(formatter)
    logger.addHandler(fh)

    return logger


def process_tasks(task_queue):

    logger = create_logger()
    proc = os.getpid()

    while not task_queue.empty():
        try:
            task_name = task_queue.get()
            logger.info("task" + str( task_name ) )
            fit_schools( int(task_name), task_name )
        except Exception as e:
            logger.error(e)

    logger.info(f"Process {proc} completed successfully")
    return True



def run():

    pickle_schools_model()
    task_queue = multiprocessing.Queue()

    # put tasks in task queue
    for entry in range(10):
        task_queue.put( str( entry ) )

    processes = []

    print(f"Running with {PROCESSES} processes!")

    start = time.time()

    for w in range(PROCESSES):
        p = multiprocessing.Process(target=process_tasks, args=(task_queue,))
        processes.append(p)
        p.start()

    for p in processes:
        p.join()

    print(f"Time taken = {time.time() - start:.10f}")




if __name__ == "__main__":
    run()
