# workflow python parallel

## References
* https://github.com/Kaggle/docker-python
* https://testdriven.io/blog/developing-an-asynchronous-task-queue-in-python/


## Commands
```
 1058  module load singularity
 1059  export SINGULARITY_BIND="/run,/ptmp,/scratch,/tmp,/opt/ohpc,${HOME}"
 1062  singularity pull docker://gcr.io/kaggle-images/python
 1091  singularity exec ./python_latest.sif python simple_task_queue_logging.py
 1092  vi process.log

sbatch simple_task_queue_logging.sbatch

```
